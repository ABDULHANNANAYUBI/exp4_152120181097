#include "Student.h"
#include <iostream>
using namespace std;

void Student::setStudent(string _name, int _id)
{
	this->name=_name;
	this->id=_id;
}

double Student::calculateGrade(){
	return double (midTermExam + finalExam) / 2;
}

void Student::print()
{

	cout<<"Name : "<<name<<endl;
	cout<<"ID   : "<<id<<endl;
	cout<<"Grade   : "<<calculateGrade()<<endl;

}
